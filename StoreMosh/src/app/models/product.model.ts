export interface ProductModel {
  title: string;
  price: number;
  category: string;
  imageUrl: string;
}
