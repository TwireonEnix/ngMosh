import { CategoryService } from './../../category.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ProductModel } from 'src/app/models/product.model';
import { ProductService } from 'src/app/product.service';

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.css']
})
export class ProductFormComponent implements OnInit {
  categories$;
  @ViewChild('price')
  price;

  constructor(
    private categoryService: CategoryService,
    private productService: ProductService
  ) {}

  ngOnInit() {
    this.categories$ = this.categoryService.getCategories();
    console.log(this.price);
  }

  save(product: ProductModel) {
    this.productService.create(product);
  }
}
