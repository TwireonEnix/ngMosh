import { AuthService } from './../auth.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserModel } from '../models/user.model';

@Component({
  selector: 'app-bs-navbar',
  templateUrl: './bs-navbar.component.html',
  styleUrls: ['./bs-navbar.component.css']
})
export class BsNavbarComponent implements OnInit {
  /* creación de un observable para hacer su destrucción más limpia sin la interfaz OnDestroy,
  por convención los observables se ponen con un $ */
  // user$: Observable<firebase.User>;

  appUser: UserModel;

  constructor(public authService: AuthService) {}

  ngOnInit() {
    /* guardar el observable en una variable con el tipo de dato que arroja, accesando a su valor con el pipe async
    a través del template con user$ | async as user, con esto con se evita la suscripción explícita al observable
    eliminados para crear el objeto directamente en el servicio (separation of concerns) */
    // this.user$ = this.afAuth.authState;
    this.authService.appUser$.subscribe(appUser => (this.appUser = appUser));
  }

  onLogout() {
    this.authService.logout();
  }
}
