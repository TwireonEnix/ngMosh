import { map, switchMap } from 'rxjs/operators';
import { AuthService } from './auth.service';
import { CanActivate } from '@angular/router';
import { Injectable } from '@angular/core';
import { UserService } from './user.service';
import { Observable } from 'rxjs';

@Injectable()
export class AdminAuthGuard implements CanActivate {
  constructor(
    private authService: AuthService,
    private userService: UserService
  ) {}

  /**  You can use pipes to link operators together.
   *  Pipes let you combine multiple functions into a single function.
   * The pipe() function takes as its arguments the functions you want to combine
   * and returns a new function that, when executed,
   * runs the composed functions in sequence. */
  canActivate(): Observable<boolean> {
    return this.authService.user$.pipe(
      switchMap(user => this.userService.get(user.uid).valueChanges()),
      map(appUser => appUser.isAdmin)
    );
  }
}
